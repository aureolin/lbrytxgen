## LBRY Transaction Generator
Due to the lack of options with LBRY wallets, I built the transaction generator as a tool to import WIF keys for vanity addresses and dumped private keys from lbrycd, to be able to generate sigend transactions which can be sent directly to the blockchain using the lbrycd sendrawtransaction command.

### Requirements
* JDK 1.8 u101 or higher
* Apache Maven 3.5

### Build
`mvn clean compile assembly:single`

### Run
`java -jar target\lbrytxgen-0.1-jar-with-dependencies.jar`