/*
 * Copyright 2017 Aureolin.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.aureolin.labs.lbrytxgen.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.bitcoinj.core.Base58;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.math.ec.ECPoint;
import java.util.Arrays;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import co.aureolin.labs.lbrytxgen.model.Address;

public final class Utils {
	
	public static DecimalFormat DecimalFormat = new DecimalFormat("#,##0.########");
    
    public static DecimalFormat SimpleZeroDecimalFormat = new DecimalFormat("#,##0.00######");
    
    public static DecimalFormat CurrencyDecimalFormat = new DecimalFormat("#,##0.00");
    
    public static DecimalFormat ZeroDecimalFormat = new DecimalFormat("#,##0.00000000");
	
    public static final X9ECParameters CURVE_PARAMS;
    
    public static final ECDomainParameters CURVE;
    
    public static final BigInteger HALF_CURVE_ORDER;
    
    public static final SecureRandom SR;
    
    private static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    
    public static final String PRIVKEY_MAP_KEY = "privateKeyWIF";
    
    public static final String PUBKEY_MAP_KEY = "publicKey";
    
    public static final String ADDRESS_MAP_KEY = "address";

    static {
        CURVE_PARAMS = SECNamedCurves.getByName("secp256k1");
        CURVE = new ECDomainParameters(CURVE_PARAMS.getCurve(), CURVE_PARAMS.getG(), CURVE_PARAMS.getN(), CURVE_PARAMS.getH());
        HALF_CURVE_ORDER = CURVE_PARAMS.getN().shiftRight(1);
        SR = new SecureRandom();
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }
    
    public static Map<String, String> generatePrivWIFPubKeyAndAddress() throws NoSuchAlgorithmException, IOException {
        Map<String, String> result = new HashMap<String, String>();
        
        byte[] privKeyBytes = new byte[32];
        SR.nextBytes(privKeyBytes);
        
        String privateKeyHex = bytesToHex(privKeyBytes);
        String privateKeyAndVersion = String.format("80%s", privateKeyHex);
        
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(hexToBytes(privateKeyAndVersion));
        byte[] firstSha = md.digest();        
        byte[] secondSha = md.digest(firstSha);
        String secondShaHex = bytesToHex(secondSha);
        String checksum = secondShaHex.substring(0, 8).toUpperCase();
        //System.out.println("Checksum = " + checksum);
        
        String keyWithChecksum = privateKeyAndVersion + checksum;
        //System.out.println("Key With Checksum = " + keyWithChecksum);
        
        String privateKeyWIF = Base58.encode(hexToBytes(keyWithChecksum));
        //System.out.println("Private Key = " + privateKeyWIF);
        result.put(PRIVKEY_MAP_KEY, privateKeyWIF);
        
        BigInteger privateKeyBN = new BigInteger(1, privKeyBytes);
        ECPoint curvePt = CURVE.getG().multiply(privateKeyBN);
        @SuppressWarnings("deprecation")
        byte[] publicKeyBytes = curvePt.getEncoded();
        String publicKeyHex = bytesToHex(publicKeyBytes);
        result.put(PUBKEY_MAP_KEY, publicKeyHex);
        //System.out.println("Public Key Hex = " + publicKeyHex);
        
        byte[] addressBytes = sha256hash160(publicKeyBytes);
        String addressHex = bytesToHex(addressBytes);
        //System.out.println("Address Hex = " + addressHex);
        
        byte[] addressHashAndBytes = new byte[addressBytes.length + 1];
        addressHashAndBytes[0] = (byte)0x00;
        System.arraycopy(addressBytes, 0, addressHashAndBytes, 1, addressBytes.length);
        
        md.reset();
        md.update(addressHashAndBytes);
        byte[] first = md.digest();
        byte[] second = md.digest(first);
        String shaAddressHex = bytesToHex(second);
        String addressChecksum = shaAddressHex.substring(0, 8);
        
        String unencodedAddress = "00" + addressHex + addressChecksum;
        //System.out.println("Unencoded Address = " + unencodedAddress);
        String address = Base58.encode(hexToBytes(unencodedAddress));
        //System.out.println("Address = " + address);
        result.put(ADDRESS_MAP_KEY, address);
        
        return result;
    }
    
    // Address version
    // 85 = 0x55 - LBRY
    public static String getAddressFromPublicKeyBytes(byte[] publicKeyBytes, int addressVersion) {
        byte[] addressBytes = sha256hash160(publicKeyBytes);
        String addressHex = bytesToHex(addressBytes);
        
        byte[] addressHashAndBytes = new byte[addressBytes.length + 1];
        addressHashAndBytes[0] = (byte)addressVersion;
        System.arraycopy(addressBytes, 0, addressHashAndBytes, 1, addressBytes.length);
        
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(addressHashAndBytes);
            byte[] first = md.digest();
            byte[] second = md.digest(first);
            String shaAddressHex = bytesToHex(second);
            String addressChecksum = shaAddressHex.substring(0, 8);
            
            String unencodedAddress = Utils.bytesToHex(new byte[] { (byte)addressVersion }) + addressHex + addressChecksum;
            return Base58.encode(hexToBytes(unencodedAddress));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
    
    public static byte[] doubleDigest(byte[] input) {
        byte[] secondSha = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(input);
            byte[] firstSha = md.digest();        
            secondSha = md.digest(firstSha);
        } catch (Exception ex) {
            // Failed
        }
        
        return secondSha;
    }
    
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    
    public static byte[] hexToBytes(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    
    public static byte[] sha256hash160(byte[] input) {
        try {
            byte[] sha256 = MessageDigest.getInstance("SHA-256").digest(input);
            RIPEMD160Digest digest = new RIPEMD160Digest();
            digest.update(sha256, 0, sha256.length);
            byte[] out = new byte[20];
            digest.doFinal(out, 0);
            return out;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);  // Cannot happen.
        }
    }

    public static String encrypt(String value, String encryptionKey) {
        String result = null;
        try {
            byte[] key = encryptionKey.getBytes("UTF8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            byte[] valueBytes = value.getBytes("UTF8");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            result = Base64.getEncoder().encodeToString(cipher.doFinal(valueBytes));
        } catch (InvalidKeyException | UnsupportedEncodingException
                | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException
                | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
        
        return result;
    }
    
    public static String decrypt(String value, String encryptionKey) {
        String result = null;
        try {
            byte[] key = encryptionKey.getBytes("UTF8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            byte[] valueBytes = Base64.getDecoder().decode(value);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC"); // cipher is not thread safe
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            result = new String(cipher.doFinal(valueBytes), Charset.forName("UTF8"));
        } catch (InvalidKeyException | UnsupportedEncodingException
                | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException
                | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }
        
        return result;
    }
    
    public static String getFormattedValue(long value) {
        BigDecimal bigDecimalValue = getValueDecimal(value);
        return getFormattedValue(bigDecimalValue);
    }
    
    public static String getConvertedValue(BigDecimal value, float rate, String currency) {
        BigDecimal convertedValue = value.multiply(BigDecimal.valueOf(rate));
        return String.format("%s %s", CurrencyDecimalFormat.format(convertedValue), currency);
    }
    
    public static String getApproxConvertedValue(BigDecimal value, float rate, String currency) {
        BigDecimal convertedValue = value.multiply(BigDecimal.valueOf(rate));
        return String.format("\u2248 %s %s", CurrencyDecimalFormat.format(convertedValue), currency);
    }
    
    public static String getFormattedValue(BigDecimal value) {
        return (value.unscaledValue().longValue() == 0) ? "0.00 BTC" : DecimalFormat.format(value) + " BTC";
    }
    
    public static String getZeroFormattedValue(long value) {
        BigDecimal bigDecimalValue = getValueDecimal(value);
        return getZeroFormattedValue(bigDecimalValue);
    }
    
    public static String getZeroFormattedValue(BigDecimal value) {
        return (value.unscaledValue().longValue() == 0) ? "0.00 BTC" : ZeroDecimalFormat.format(value) + " BTC";
    }

    public static long getUnscaledValue(BigDecimal value) {
        return (value == null) ? 0 : value.unscaledValue().longValue();
    }
    
    public static BigDecimal getValueDecimal(long value) {
        return new BigDecimal(BigInteger.valueOf(value), 8);
    }
    
    public static String getJsonText(String key, ObjectNode node) {
        return (node.has(key)) ? node.get(key).asText().trim() : null;
    }
    public static long getJsonLong(String key, ObjectNode node) {
        return (node.has(key)) ? node.get(key).asLong() : 0;
    }
    public static int getJsonInt(String key, ObjectNode node) {
        return (node.has(key)) ? node.get(key).asInt() : 0;
    }
    
    public static boolean getJsonBoolean(String key, JsonNode node) {
        return (node.has(key)) ? node.get(key).asBoolean() : false;
    }
    public static String getJsonText(String key, JsonNode node) {
        return (node.has(key)) ? node.get(key).asText().trim() : null;
    }
    public static long getJsonLong(String key, JsonNode node) {
        return (node.has(key)) ? node.get(key).asLong() : 0;
    }
    public static int getJsonInt(String key, JsonNode node) {
        return (node.has(key)) ? node.get(key).asInt() : 0;
    }
    public static float getJsonFloat(String key, JsonNode node) {
        return (node.has(key)) ? Double.valueOf(node.get(key).asDouble()).floatValue() : 0;
    }
    public static String[] getJsonStringArray(String key, JsonNode node) {
        if (!node.has(key)) {
            return new String[0];
        }
        
        List<String> items = new ArrayList<String>();
        JsonNode addressesNode = node.get(key);
        if (addressesNode != null) {
            for (JsonNode itemNode : addressesNode) {
                String value = itemNode.asText();
                if (value != null && !items.contains(value)) {
                    items.add(value);
                }
            }
        }
        return items.toArray(new String[items.size()]);
    }
    public static List<String> getJsonAddressList(String key, JsonNode node) {
        List<String> addressList = new ArrayList<String>();
        JsonNode addressNodes = node.get(key);
        if (addressNodes != null && addressNodes.isArray()) {
            for (JsonNode addressNode : addressNodes) {
                String base58address = addressNode.asText();
                if (!addressList.contains(base58address)) {
                    addressList.add(base58address);
                }
            }
        }
        return addressList;
    }
    
    public static String join(List<String> items, String separator) {
        StringBuilder sb = new StringBuilder();
        String delim = "";
        for (int i = 0; i < items.size(); i++) {
            sb.append(delim).append(items.get(i));
            delim = separator;
        }
        return sb.toString();
    }
    
    public static String joinAddresses(List<Address> addresses, String separator) {
        StringBuffer sb = new StringBuffer();
        String delim = "";
        for (int i = 0; i < addresses.size(); i++) {
            sb.append(delim).append(addresses.get(i).getAddress());
            delim = separator;
        }
        
        return sb.toString();
    }
    
    public static String padDecimalString(String strValue) {
        int pointIndex = strValue.indexOf('.');
        if (pointIndex > -1) {
            int numDecimals = strValue.substring(pointIndex + 1).length();
            while (numDecimals < 8) {
                strValue = strValue + "0";
                numDecimals = strValue.substring(pointIndex + 1).length();
            }
        } else {
            strValue = strValue + ".00000000";
        }
    
        return strValue;
    }
    
    public static SecretKeySpec generatePasswordKey(String password) {
        try {
            byte[] key = password.getBytes("UTF8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            
            return new SecretKeySpec(key, "AES");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }        
    }
}
