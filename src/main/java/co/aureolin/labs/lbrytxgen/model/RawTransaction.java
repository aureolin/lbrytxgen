/*
 * Copyright 2017 Aureolin.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.aureolin.labs.lbrytxgen.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Base58;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERSequenceGenerator;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;

import co.aureolin.labs.lbrytxgen.model.RawInput.ScriptSig;
import co.aureolin.labs.lbrytxgen.model.RawOutput.ScriptPubKey;
import co.aureolin.labs.lbrytxgen.util.Utils;

public class RawTransaction {
    private static final byte SIGHASH_ALL = 1;    
    public static final byte OP_FALSE = 0;
    public static final byte OP_TRUE = 0x51;
    public static final byte OP_PUSHDATA1 = 0x4c;
    public static final byte OP_PUSHDATA2 = 0x4d;
    public static final byte OP_PUSHDATA4 = 0x4e;
    public static final byte OP_DUP = 0x76;//Duplicates the top stack item.
    public static final byte OP_DROP = 0x75;
    public static final byte OP_HASH160 = (byte) 0xA9;//The input is hashed twice: first with SHA-256 and then with RIPEMD-160.
    public static final byte OP_VERIFY = 0x69;//Marks transaction as invalid if top stack value is not true. True is removed, but false is not.
    public static final byte OP_EQUAL = (byte) 0x87;//Returns 1 if the inputs are exactly equal, 0 otherwise.
    public static final byte OP_EQUALVERIFY = (byte) 0x88;//Same as OP_EQUAL, but runs OP_VERIFY afterward.
    public static final byte OP_CHECKSIG = (byte) 0xAC;//The entire transaction's outputs, inputs, and script (from the most recently-executed OP_CODESEPARATOR to the end) are hashed. The signature used by OP_CHECKSIG must be a valid signature for this hash and public key. If it is, 1 is returned, 0 otherwise.
    public static final byte OP_CHECKSIGVERIFY = (byte) 0xAD;
    public static final byte OP_NOP = 0x61;
    
    private String transactionId;

    private long version;

    private long locktime;

    private List<RawInput> rawInputs;

    private List<RawOutput> rawOutputs;

    public static String flipByteOrder(String hex) {
        byte[] bytes = Utils.hexToBytes(hex);
        byte[] reversed = new byte[bytes.length];
        for (int i = (bytes.length - 1), j = 0; i >= 0 && j < reversed.length; i--, j++) {
            reversed[j] = bytes[i];
        }
        return Utils.bytesToHex(reversed);
    }

    public static String returnBytesHex(String hex, long byteCount,
            boolean reverse) {
        String bytes = substring(hex, 0, byteCount * 2);
        return (!reverse) ? bytes : flipByteOrder(bytes);
    }

    public static String decimalToBytesHex(long decimal, int numBytes, boolean reverse) {
        String hex = Long.toString(decimal, 16);
        if (hex.length() % 2 != 0) {
            hex = "0" + hex;
        }
        int padLength = numBytes * 2;
        while (hex.length() < padLength) {
            hex = "0" + hex;
        }
        return (reverse) ? flipByteOrder(hex) : hex;
    }

    public static long getVint(String hex) {
        long decimal = Long.parseLong(hex, 16);
        int numBytes = Long.valueOf((decimal < 253) ? 0 : 2 ^ (decimal - 253))
                .intValue();
        return (numBytes == 0) ? decimal : Long.parseLong(
                returnBytesHex(hex, numBytes, true), 16);
    }

    public static String encodeVint(long value) {
        //String hex = Long.toString(value, 16);
        String hint;
        int numBytes;
        BigInteger maxLong = new BigInteger("18446744073709551615");
        if (value < 253) {
            hint = decimalToBytesHex(value, 1, false);
            numBytes = 0;
        } else if (value < 65535) {
            hint = "fd";
            numBytes = 2;
        } else if (value < 4294967295L) {
            hint = "fe";
            numBytes = 4;
        } else if (maxLong.compareTo(BigInteger.valueOf(value)) > 1) {
            hint = "ff";
            numBytes = 8;
        } else {
            return "";
        }
        return (numBytes == 0) ? hint : hint + decimalToBytesHex(value, numBytes, true);
    }

    public static String encodeInputs(List<RawInput> rawInputs, int inputCount) {
        String inputs = "";
        for (int i = 0; i < inputCount; i++) {
            RawInput rawInput = rawInputs.get(i);

            String txid;
            String vout;
            long scriptSize;
            String scriptVarInt;
            String scriptSig;
            if (rawInput.getCoinbase() != null) {
                // Coinbase
                txid = "0000000000000000000000000000000000000000000000000000000000000000";
                vout = "ffffffff";
                scriptSize = rawInput.getCoinbase().length() / 2; // Decimal
                                                                  // number of
                                                                  // bytes
                scriptVarInt = encodeVint(scriptSize);
                scriptSig = scriptVarInt + rawInput.getCoinbase();
            } else {
                // Regular transaction
                txid = flipByteOrder(rawInput.getTransactionId());
                vout = decimalToBytesHex(rawInput.getVout(), 4, true);
                scriptSize = rawInput.getScriptSig().getHex().length() / 2; // decimal
                                                                            // number
                                                                            // of
                                                                            // bytes
                scriptVarInt = encodeVint(scriptSize);
                scriptSig = scriptVarInt + rawInput.getScriptSig().getHex();
            }
            String sequence = decimalToBytesHex(rawInput.getSequence(), 4, true);

            inputs += txid + vout + scriptSig + sequence;
        }

        return inputs;
    }

    public static String encodeOutputs(List<RawOutput> rawOutputs,
            int outputCount) {
        if (rawOutputs.size() == 0) {
            return (outputCount == 0) ? "" : "-1";
        }

        String outputs = "";
        for (int i = 0; i < outputCount; i++) {
            RawOutput rawOutput = rawOutputs.get(i);
            long satoshis = rawOutput.getValue();
            String amount = decimalToBytesHex(satoshis, 8, false);
            amount = flipByteOrder(amount);

            long scriptSize = rawOutput.getScriptPubKey().getHex().length() / 2; // number
                                                                                 // of
                                                                                 // bytes
            String scriptVarInt = encodeVint(scriptSize);
            String scriptPubKey = rawOutput.getScriptPubKey().getHex();

            outputs += amount + scriptVarInt + scriptPubKey;
        }
        return outputs;
    }

    public static String encodeTransaction(RawTransaction rawTransaction) {
        String bytes = decimalToBytesHex(rawTransaction.getVersion(), 4, true);
        String encodedVersion = bytes;

        int inputCount = rawTransaction.getRawInputs().size();
        String encodedInputs = encodeVint(inputCount) + ((inputCount > 0) ?
        	encodeInputs(rawTransaction.getRawInputs(), inputCount) : "");

        int outputCount = rawTransaction.getRawOutputs().size();
        String encodedOutputs = encodeVint(outputCount) + ((outputCount > 0) ?
        	encodeOutputs(rawTransaction.getRawOutputs(), outputCount) : "");

        // Transaction lock time
        String encodedLocktime = decimalToBytesHex(rawTransaction.getLocktime(), 4, true);

        return encodedVersion + encodedInputs + encodedOutputs + encodedLocktime;
    }
    public static RawTransaction createRawTransaction(List<RawInput> rawInputs, List<RawOutput> rawOutputs, String addressVersion) {
        RawTransaction rawTransaction = new RawTransaction();
        rawTransaction.setVersion(1);
        rawTransaction.setRawInputs(rawInputs);

        for (int i = 0; i < rawOutputs.size(); i++) {
            RawOutput rawOutput = rawOutputs.get(i);
            byte[] pubKeyBytes = null;
            try {
                pubKeyBytes = Base58.decodeChecked(rawOutput.getAddress());
            } catch (AddressFormatException e) {
                //Log.e("RawTransaction", "Address format invalid", e);
                return null;
            }
            String pubKeyHex = Utils.bytesToHex(pubKeyBytes);
            String hash = substring(pubKeyHex, 2, 40);

            // OP_DUP OP_HASH160 <pubKeyHash> OP_EQUALVERIFY OP_CHECKSIG
            String scriptPubKey = String.format("76a914%s88ac", hash);
            ScriptPubKey spKey = new ScriptPubKey();
            spKey.setHex(scriptPubKey);
            rawOutput.setScriptPubKey(spKey);
        }
        rawTransaction.setRawOutputs(rawOutputs);
        rawTransaction.setLocktime(0);
        return rawTransaction;
    }
    
        
    public static String signRawTransaction(RawTransaction rawTransaction, String[] wifKeys, String[] pubKeys) {
        List<RawInput> txRawInputs = rawTransaction.getRawInputs();
        String sigHashCode = "01000000";
        for (int i = 0; i < txRawInputs.size(); i++) {
            RawTransaction copy = copyRawTransaction(rawTransaction);
            RawInput rawInput = txRawInputs.get(i);
            
          
            for (int j = 0; j < copy.getRawInputs().size(); j++) {
                copy.getRawInputs().get(j).getScriptSig().setAsm("");
                copy.getRawInputs().get(j).getScriptSig().setHex("");
            }

            byte[] privKeyBytes = new byte[32];
            byte[] checkedPrivKeyBytes = null;
            byte[] publicKeyBytes = Utils.hexToBytes(pubKeys[i]);
            BigInteger privateKeyBN = null;
            
            // Get the actual private key from the WIF key
            try {
            	System.out.println(pubKeys[i] + "***" + wifKeys[i]);
                checkedPrivKeyBytes = Base58.decodeChecked(wifKeys[i]);
                System.arraycopy(checkedPrivKeyBytes, 1, privKeyBytes, 0, privKeyBytes.length);
                privateKeyBN = new BigInteger(1, privKeyBytes);
            } catch (AddressFormatException e) {
                return "";
            }

            copy.getRawInputs().get(i).getScriptSig().setHex(rawInput.getScriptSig().getHex());
            
            // Encode the transaction, convert to a raw byte sting,
            // and calculate a double sha256 hash for this input
            byte[] txCopyBytes = Utils.hexToBytes(encodeTransaction(copy));
            byte[] sigHash = Utils.hexToBytes(sigHashCode);
            byte[] copyTxEncoded = null;
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.write(txCopyBytes);
                baos.write(sigHash);
                copyTxEncoded = baos.toByteArray();
                baos.close();
            } catch (IOException e) {
                return "";
            }
            
            byte[] messageHash = Utils.doubleDigest(copyTxEncoded);
            rawTransaction.getRawInputs().get(i).getScriptSig().setHex(Utils.bytesToHex(messageHash));

            ECDSASigner signer = new ECDSASigner();
            ECPrivateKeyParameters privKey = new ECPrivateKeyParameters(privateKeyBN, Utils.CURVE);
            signer.init(true, privKey);
            BigInteger[] components = signer.generateSignature(messageHash);
            BigInteger r = components[0];
            BigInteger s = components[1];
            // Canonicalised
            if (s.compareTo(Utils.HALF_CURVE_ORDER) > 0) {
                s = Utils.CURVE.getN().subtract(s);
            }

            byte[] derSignature = null;
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DERSequenceGenerator seq = new DERSequenceGenerator(bos);
                seq.addObject(new ASN1Integer(r));
                seq.addObject(new ASN1Integer(s));
                seq.close();

                derSignature = bos.toByteArray();
                bos.close();
            } catch (IOException e) {
                return "";
            }
            
            byte[] signature = derSignature;
            byte[] signatureAndHashType = new byte[signature.length + 1];
            System.arraycopy(signature, 0, signatureAndHashType, 0, signature.length);
            signatureAndHashType[signatureAndHashType.length - 1] = SIGHASH_ALL;
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream(signatureAndHashType.length + publicKeyBytes.length + 2);
            try {
                writeBytes(signatureAndHashType, baos);
                writeBytes(publicKeyBytes, baos);
                baos.close();
            } catch (IOException e) {
            throw new RuntimeException(e);
            }
            byte[] scriptSigHex = baos.toByteArray();
            rawTransaction.getRawInputs().get(i).getScriptSig().setHex(Utils.bytesToHex(scriptSigHex));
        }

        // Re-encode the signed raw transaction
        return encodeTransaction(rawTransaction);
    }
    
    private static void writeBytes(byte[] data, ByteArrayOutputStream baos) throws IOException {
        if (data.length < OP_PUSHDATA1) {
            baos.write(data.length);
        } else if (data.length < 0xff) {
            baos.write(OP_PUSHDATA1);
            baos.write(data.length);
        } else if (data.length < 0xffff) {
            baos.write(OP_PUSHDATA2);
            baos.write(data.length & 0xff);
            baos.write((data.length >> 8) & 0xff);
        } else {
            baos.write(OP_PUSHDATA4);
            baos.write(data.length & 0xff);
            baos.write((data.length >> 8) & 0xff);
            baos.write((data.length >> 16) & 0xff);
            baos.write((data.length >>> 24) & 0xff);
        }
        baos.write(data);
    }

    public static String encodeSignature(BigInteger r, BigInteger s) {
        String rh = padLeft(r.toString(16), "0", 64);
        String sh = padLeft(s.toString(16), "0", 64);

        // Check the first byte
        byte[] rhb = Utils.hexToBytes(rh);
        byte[] shb = Utils.hexToBytes(sh);
        if (rhb[0] != 0) {
            rh = "00" + rh;
        }
        if (shb[0] != 0) {
            sh = "00" + sh;
        }

        r = new BigInteger(rh, 16);
        s = new BigInteger(sh, 16);
        String derSig = "30"
                + decimalToBytesHex((4 + ((rh.length() + sh.length()) / 2)), 1, false) + "02"
                + decimalToBytesHex(rh.length() / 2, 1, false) + rh + "02"
                + decimalToBytesHex(sh.length() / 2, 1, false) + sh + "01";

        return derSig;
    }

    private static String padLeft(String s, String padChar, int len) {
        while (s.length() < len) {
            s = padChar + s;
        }
        return s;
    }

    public static RawTransaction copyRawTransaction(RawTransaction rawTransaction) {
        RawTransaction copy = new RawTransaction();
        copy.setTransactionId(rawTransaction.getTransactionId());
        copy.setVersion(rawTransaction.getVersion());
        copy.setLocktime(rawTransaction.getLocktime());
        
        List<RawInput> copiedInputs = new ArrayList<RawInput>();
        List<RawOutput> copiedOutputs = new ArrayList<RawOutput>();
        for (RawInput input : rawTransaction.getRawInputs()) {
            RawInput rawInput = new RawInput();
            rawInput.setTransactionId(input.getTransactionId());
            rawInput.setCoinbase(input.getCoinbase());
            rawInput.setSequence(input.getSequence());
            rawInput.setTransactionId(input.getTransactionId());
            rawInput.setVout(input.getVout());
            
            ScriptSig scriptSig = new ScriptSig();
            scriptSig.setAsm(input.getScriptSig().getAsm());
            scriptSig.setHex(input.getScriptSig().getHex());
            rawInput.setScriptSig(scriptSig);
            
            copiedInputs.add(rawInput);
        }
        for (RawOutput output : rawTransaction.getRawOutputs()) {
            RawOutput rawOutput = new RawOutput();
            rawOutput.setAddress(output.getAddress());
            rawOutput.setValue(output.getValue());
            rawOutput.setVout(output.getVout());
            
            ScriptPubKey scriptPubKey = new ScriptPubKey();
            scriptPubKey.setAsm(output.getScriptPubKey().getAsm());
            scriptPubKey.setHex(output.getScriptPubKey().getHex());
            rawOutput.setScriptPubKey(scriptPubKey);
            
            copiedOutputs.add(rawOutput);
        }
        
        copy.setRawInputs(copiedInputs);
        copy.setRawOutputs(copiedOutputs);

        return copy;
    }

    public static String substring(String s, long start, long length) {
        return s.substring(Long.valueOf(start).intValue(),
                Long.valueOf(Math.min(start + length, s.length())).intValue());
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getLocktime() {
        return locktime;
    }

    public void setLocktime(long blocktime) {
        this.locktime = blocktime;
    }

    public List<RawInput> getRawInputs() {
        return rawInputs;
    }

    public void setRawInputs(List<RawInput> rawInputs) {
        this.rawInputs = rawInputs;
    }

    public List<RawOutput> getRawOutputs() {
        return rawOutputs;
    }

    public void setRawOutputs(List<RawOutput> rawOutputs) {
        this.rawOutputs = rawOutputs;
    }
}
