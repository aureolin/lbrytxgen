/*
 * Copyright 2017 Aureolin.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.aureolin.labs.lbrytxgen.model;

import co.aureolin.labs.lbrytxgen.model.Unspent;

public class RawInput {
    private String transactionId;
    
    private String coinbase;
    
    private long vout;
    
    private ScriptSig scriptSig;
    
    private long sequence;
    
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCoinbase() {
        return coinbase;
    }

    public void setCoinbase(String coinbase) {
        this.coinbase = coinbase;
    }

    public long getVout() {
        return vout;
    }

    public void setVout(long vout) {
        this.vout = vout;
    }

    public ScriptSig getScriptSig() {
        return scriptSig;
    }

    public void setScriptSig(ScriptSig scriptSig) {
        this.scriptSig = scriptSig;
    }    

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    public static class ScriptSig {
        private String asm;
        
        private String hex;

        public String getAsm() {
            return asm;
        }

        public void setAsm(String asm) {
            this.asm = asm;
        }
        
        public String getHex() {
            return hex;
        }

        public void setHex(String hex) {
            this.hex = hex;
        }
    }
    
    public static RawInput rawInputFromUnspent(Unspent unspent) {
        ScriptSig scriptSig = new ScriptSig();
        scriptSig.setHex(unspent.getScriptHex());
        scriptSig.setAsm(unspent.getScript());
        System.out.println(scriptSig.getHex());
        System.out.println(scriptSig.getAsm());
        
        RawInput rawInput = new RawInput();
        rawInput.setTransactionId(unspent.getTransactionId());
        rawInput.setScriptSig(scriptSig);
        rawInput.setVout(unspent.getOutputIndex());
        
        return rawInput;
    }
}
