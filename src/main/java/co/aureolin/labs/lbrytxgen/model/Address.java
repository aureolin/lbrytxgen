/*
 * Copyright 2017 Aureolin.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.aureolin.labs.lbrytxgen.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import co.aureolin.labs.lbrytxgen.util.Utils;

public class Address {
	
	private int id;
	
	private String address;
	
	private String base58WIF;
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBase58WIF() {
		return base58WIF;
	}

	public void setBase58WIF(String base58wif) {
		base58WIF = base58wif;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public JsonNode toJsonNode(ObjectMapper mapper) {
		ObjectNode node = mapper.createObjectNode();
		NumericNode addressIdNode = mapper.createObjectNode().numberNode(id);
		TextNode addressValueNode = mapper.createObjectNode().textNode(address);
		TextNode wifValueNode = mapper.createObjectNode().textNode(base58WIF);
		node.set("id", addressIdNode);
		node.set("address", addressValueNode);
		node.set("wif", wifValueNode);
		return node;
	}
	
	public static Address fromJsonNode(JsonNode node) {
		Address address = new Address();
		address.setId(Utils.getJsonInt("id", node));
		address.setAddress(Utils.getJsonText("address", node));
		address.setBase58WIF(Utils.getJsonText("wif", node));
		
		return address;
	}
	
	@Override
	public String toString() {
		return String.format("(%d) %s", id, address);
	}
	
	@Override
	public int hashCode() {
		return address.hashCode();
	}
}
