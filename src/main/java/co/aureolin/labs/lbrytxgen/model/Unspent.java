/*
 * Copyright 2017 Aureolin.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.aureolin.labs.lbrytxgen.model;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import co.aureolin.labs.lbrytxgen.util.Utils;

public class Unspent {
    private String transactionId;
    
    private int outputIndex;
    
    private long value;
    
    private List<String> addresses;
    
    private String joinedAddresses;
    
    private String script;
    
    private String scriptHex;
    
    private String scriptType;
    
    private int requiredSignatures;
    
    private boolean spent;
    
    private int confirmations;
    
    private String address;
    
    private int addressId;
    
    private String decimalValue;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getOutputIndex() {
        return outputIndex;
    }

    public void setOutputIndex(int outputIndex) {
        this.outputIndex = outputIndex;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public List<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    public String getJoinedAddresses() {
        return joinedAddresses;
    }

    public void setJoinedAddresses(String joinedAddresses) {
        this.joinedAddresses = joinedAddresses;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getScriptHex() {
        return scriptHex;
    }

    public void setScriptHex(String scriptHex) {
        this.scriptHex = scriptHex;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

    public int getRequiredSignatures() {
        return requiredSignatures;
    }

    public void setRequiredSignatures(int requiredSignatures) {
        this.requiredSignatures = requiredSignatures;
    }

    public boolean isSpent() {
        return spent;
    }

    public void setSpent(boolean spent) {
        this.spent = spent;
    }

    public int getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(int confirmations) {
        this.confirmations = confirmations;
    }
    
    public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDecimalValue() {
		return decimalValue;
	}

	public void setDecimalValue(String decimalValue) {
		this.decimalValue = decimalValue;
	}
	
	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	
	public JsonNode toJsonNode(ObjectMapper mapper) {
		ArrayNode addressesNode = mapper.createArrayNode();
		addressesNode.add(mapper.createObjectNode().textNode(addresses.get(0)));
		
		ObjectNode node = mapper.createObjectNode();
		node.set("address_id", mapper.createObjectNode().numberNode(addressId));
		node.set("transaction_hash", mapper.createObjectNode().textNode(transactionId));
		node.set("output_index", mapper.createObjectNode().numberNode(outputIndex));
		node.set("value", mapper.createObjectNode().numberNode(value));
		node.set("addresses", addressesNode);
		node.set("script", mapper.createObjectNode().textNode(script));
		node.set("script_hex", mapper.createObjectNode().textNode(scriptHex));
		node.set("required_signatures", mapper.createObjectNode().numberNode(requiredSignatures));
		node.set("spent", mapper.createObjectNode().booleanNode(spent));
		node.set("confirmations", mapper.createObjectNode().numberNode(confirmations));
		
		return node;
	}

	public static Unspent fromJsonNode(JsonNode node) {
        Unspent unspent = new Unspent();
        unspent.setAddressId(Utils.getJsonInt("address_id", node));
        unspent.setTransactionId(Utils.getJsonText("transaction_hash", node));
        unspent.setOutputIndex(Utils.getJsonInt("output_index", node));
        unspent.setValue(Utils.getJsonLong("value", node));
        unspent.setAddresses(Utils.getJsonAddressList("addresses", node));
        unspent.setScript(Utils.getJsonText("script", node));
        unspent.setScriptHex(Utils.getJsonText("script_hex", node));
        unspent.setScriptType(Utils.getJsonText("script_type", node));
        unspent.setRequiredSignatures(Utils.getJsonInt("required_signatures", node));
        unspent.setSpent(Utils.getJsonBoolean("spent", node));
        unspent.setConfirmations(Utils.getJsonInt("confirmations", node));
        
        unspent.setAddress(unspent.getAddresses().get(0));
        unspent.setDecimalValue(Utils.ZeroDecimalFormat.format(Utils.getValueDecimal(unspent.getValue())));
        
        return unspent;
    }
	
	@Override
	public String toString() {
		return String.format("%s:%s", transactionId, outputIndex);
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode(); 
	}
}
