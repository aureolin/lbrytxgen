/*
 * Copyright 2017 Aureolin.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.aureolin.labs.lbrytxgen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Base58;
import org.bouncycastle.math.ec.ECPoint;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import co.aureolin.labs.lbrytxgen.model.Address;
import co.aureolin.labs.lbrytxgen.model.RawInput;
import co.aureolin.labs.lbrytxgen.model.RawOutput;
import co.aureolin.labs.lbrytxgen.model.RawTransaction;
import co.aureolin.labs.lbrytxgen.model.Recipient;
import co.aureolin.labs.lbrytxgen.model.Unspent;
import co.aureolin.labs.lbrytxgen.util.Utils;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

/**
 * Main class and application entry point.
 * 
 * TODO: Fix the UI initialisation spaghetti code. Just learnt JavaFX in a couple of days.
 * 
 * WARNING: Use this code at your own risk. I am not liable for any loss of coins, money, hair
 * or other valuables that may occur as a result of using this application. Always make sure to
 * double-check the outputs for the raw transaction using decoderawtransaction before sending. 
 * 
 * Got questions of feedback? You can find me on the LBRY Slack as @shockr.
 * If you find this useful, LBC tips or donations to bacon25yAnhH2YZxKDt4G7DcygXD8xpM1a are welcome.
 * 
 * @author Akinwale <akinwale@aureolin.co>
 * @version 0.1
 */
public class Main extends Application {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	private static final String AppDataPath = "appdata.json";
	
	private static boolean IsFirstLoad = false;
	
	private static String[] CurrentWifKeys;
    
    private static String[] CurrentPubKeys;
    
	private static final String ZeroValue = "0.00000000";
	
	// fixed fee of 0.000113
	private static final long FixedFeeValue = 11300;
	
	// Unspent outputs base URL (Please do not abuse)
	private static final String UtxoUrl = "https://lbry.block.ng/api/v1/address/{param}/utxo";
	
	private static HashMap<String, Unspent> AllUnspents = new HashMap<String, Unspent>();
	
	private static boolean loadUtxoInProgress = false;
	
	private static JsonNode currentUtxoResult = null;
	
	private ListView<Address> addressListView = new ListView<Address>();
	
	private TableView<Unspent> utxoTableView = new TableView<Unspent>();
	
	private TableView<Recipient> recipientsTableView = new TableView<Recipient>(); 
	
	private ObservableList<Address> addresses = FXCollections.observableArrayList();
	
	private ObservableList<Unspent> unspents = FXCollections.observableArrayList();
	
	private ObservableList<Recipient> recipients = FXCollections.observableArrayList();
	
	private Button btnRefreshUtxo;
	
	private Button btnRemoveAddress;
	
	private Button btnRemoveRecipient;
	
	private Label lblUnspentValue;
	
	private Label lblOutputValue;
	
	private Label lblBalanceValue;
	
	private Label lblFeeValue;
	
	private Button btnGenerate;
	
	private TextField txtReturnAddress;
	
	private long totalUnspentValue = 0;
	
	private long totalOutputValue = 0;
	
	public static void main(String[] args) {
        launch(args);
    }
	
	/**
	 * TODO: Allow the user to set an encryption key / password?
	 */
	private static String getEncryptionKey() {
		try {
			InetAddress localhost = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(localhost);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			return sb.toString();
		} catch (Exception ex) {
			// No MAC address?
			return "keyPlaceholder";
		}
	}
	
	public void loadApplicationData() {
		JsonNode globalNode = null;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(AppDataPath)));
			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			
			globalNode = mapper.readValue(sb.toString(), JsonNode.class);
		} catch (IOException ex) {
			// could not read app data
		} finally {
			closeCloseable(reader);
		}
		
		// Parse the json data
		if (globalNode != null) {
			JsonNode walletsNode = globalNode.get("wallets");
			if (walletsNode != null && walletsNode.isArray()) {
				for (int i = 0; i < walletsNode.size(); i++) {
					Address address = Address.fromJsonNode(walletsNode.get(i));
					addresses.add(address);
				}
			}
			
			JsonNode utxoNode = globalNode.get("utxo");
			List<Unspent> loaded = new ArrayList<Unspent>();
			if (utxoNode != null && utxoNode.isArray()) {
				for (int i = 0; i < utxoNode.size(); i++) {
					Unspent unspent = Unspent.fromJsonNode(utxoNode.get(i));
					AllUnspents.put(unspent.toString(), unspent);
					loaded.add(unspent);
				}
			}
			unspents.addAll(loaded);
		}
	}
	
	public static JsonNode httpGet(String url) {
        JsonNode responseObject = null;
        String response = "";
        HttpURLConnection conn = null;
        BufferedReader reader = null;

        try {
            URL req = new URL(url);
            conn = (HttpURLConnection) req.openConnection();
            conn.setConnectTimeout(2000);
            response = readInputStream(conn.getInputStream(), reader);
            responseObject = mapper.readValue(response, JsonNode.class);
        } catch (MalformedURLException e) {
            // request failed
        } catch (IOException e) {
            // request failed
        } finally {
            closeCloseable(reader);
            disconnect(conn);
        }

        return responseObject;
    }
    
    private static void disconnect(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }
    
    private static String readInputStream(InputStream in, BufferedReader reader)
        throws IOException {
        if (reader == null) {
            reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
        }

        StringBuilder sb = new StringBuilder();
        String input;
        while ((input = reader.readLine()) != null) {
            sb.append(input);
        }

        return sb.toString();
    }
	
	public void saveApplicationData() {
		ObjectNode globalNode = mapper.createObjectNode();
		
		ArrayNode walletsNode = mapper.createArrayNode();
		for (Address address : new ArrayList<Address>(addresses)) {
			walletsNode.add(address.toJsonNode(mapper));
		}
		globalNode.set("wallets", walletsNode);
		
		// Save UTXOs
		ArrayNode utxoNode = mapper.createArrayNode();
		for (Unspent unspent : AllUnspents.values()) {
			utxoNode.add(unspent.toJsonNode(mapper));
		}
		globalNode.set("utxo", utxoNode);
		
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(AppDataPath)));
			writer.write(globalNode.toString());
		} catch (IOException ex) {
			System.out.println("Could not save user data."); // show alert maybe?
		} finally {
			closeCloseable(writer);
		}
	} 
	
	private static void closeCloseable(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (IOException ignored) {
			
		}
	}
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("LBRY Transaction Generator");
        primaryStage.setResizable(false);
        
        HBox root = new HBox();
        root.setPadding(new Insets(0, 10, 10, 10));
        root.setSpacing(10);
        
        GridPane bottom = new GridPane();
        bottom.setAlignment(Pos.BASELINE_LEFT);
        bottom.setPadding(new Insets(15, 0, 10, 0));
        bottom.setHgap(10);
        bottom.setVgap(10);
        
        // init
        addressListView.setItems(addresses);
        addressListView.setPrefHeight(200);
        addressListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        utxoTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        utxoTableView.setMaxWidth(Double.MAX_VALUE);
        recipientsTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        initTableViewWithColumns(new String[] {"Addr", "TX Hash", "Vout", "Value (LBC)" },
        		new String[] {"addressId", "transactionId", "outputIndex", "decimalValue" },
        		new int[] { 50, 160, 50, 130 },
        		utxoTableView);
        utxoTableView.setItems(unspents);
        initTableViewWithColumns(new String[] {"Address", "Amount (LBC)" },
        		new String[] { "address", "decimalValue" },
        		new int[] { 250, 120 },
        		recipientsTableView);
        recipientsTableView.setItems(recipients);

        // Build bottom
        Label lblUnspentText = new Label("Total unspent value");
        lblUnspentValue = new Label(ZeroValue);
        lblUnspentValue.setTextAlignment(TextAlignment.RIGHT);
        lblUnspentValue.setAlignment(Pos.BASELINE_RIGHT);
        lblUnspentValue.setPrefWidth(250);
        
        Label lblOutputText = new Label("Total output value");
        lblOutputValue = new Label(ZeroValue);
        lblOutputValue.setAlignment(Pos.BASELINE_RIGHT);
        lblOutputValue.setPrefWidth(250);
        
        Label lblFeeText = new Label("TX Fee");
        lblFeeValue = new Label(Utils.ZeroDecimalFormat.format(Utils.getValueDecimal(FixedFeeValue)));
        lblFeeValue.setAlignment(Pos.BASELINE_RIGHT);
        lblFeeValue.setPrefWidth(250);
        
        Label lblBalanceText = new Label("Balance / Change");
        lblBalanceValue = new Label(ZeroValue);
        lblBalanceValue.setAlignment(Pos.BASELINE_RIGHT);
        lblBalanceValue.setPrefWidth(250);
        
        displayTotals();
        
        Label lblReturnAddress = new Label("Return address");
        txtReturnAddress = new TextField();
        txtReturnAddress.setPromptText("Return address");
        
        btnGenerate = new Button("Generate Signed Transaction");
        btnGenerate.setMaxWidth(Double.MAX_VALUE);
        
        GridPane.setConstraints(lblUnspentText, 0, 0);
        GridPane.setConstraints(lblUnspentValue, 1, 0);
        GridPane.setConstraints(lblOutputText, 0, 1);
        GridPane.setConstraints(lblOutputValue, 1, 1);
        GridPane.setConstraints(lblFeeText, 0, 2);
        GridPane.setConstraints(lblFeeValue, 1, 2);
        GridPane.setConstraints(lblBalanceText, 0, 3);
        GridPane.setConstraints(lblBalanceValue, 1, 3);
        GridPane.setConstraints(lblReturnAddress, 0, 4);
        GridPane.setConstraints(txtReturnAddress, 1, 4);
        
        lblOutputText.setPrefWidth(120);
        txtReturnAddress.setPrefWidth(250);
        bottom.getChildren().add(lblUnspentText);
        bottom.getChildren().add(lblUnspentValue);
        bottom.getChildren().add(lblOutputText);
        bottom.getChildren().add(lblOutputValue);
        bottom.getChildren().add(lblFeeText);
        bottom.getChildren().add(lblFeeValue);
        bottom.getChildren().add(lblBalanceText);
        bottom.getChildren().add(lblBalanceValue);
        bottom.getChildren().add(lblReturnAddress);
        bottom.getChildren().add(txtReturnAddress);
        bottom.getChildren().add(btnGenerate);
        
        //top.getChildren().add(utxoListView);
        VBox addressGroup = buildTopVBoxGroup("Addresses", new String[] { "Add", "Remove" }, true);
        addressGroup.getChildren().add(addressListView);
        Button btnAddAddress = getVBoxGroupButton(addressGroup, 1);
        btnAddAddress.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				createAddAddressDialog(primaryStage, addresses);
			}
		});
        
        VBox left = new VBox();
        left.setFillWidth(true);
        left.setMaxWidth(410);
        left.setPrefWidth(410);
        left.setMaxHeight(Double.MAX_VALUE);
        
        VBox right = new VBox();
        right.setPrefWidth(370);
        right.setMaxHeight(Double.MAX_VALUE);
        
        VBox utxoGroup = buildTopVBoxGroup("Unspent Outputs", new String[] { "Refresh" }, true);
        utxoGroup.getChildren().add(utxoTableView);
        
        btnRemoveAddress = getVBoxGroupButton(addressGroup, 2);
        btnRemoveAddress.setDisable(true);
        btnRemoveAddress.setOnAction(new EventHandler<ActionEvent>() {
        	@Override
			public void handle(ActionEvent arg0) {
        		List<Address> selected = new ArrayList<Address>(addressListView.getSelectionModel().getSelectedItems());
        		for (int i = 0; i < selected.size(); i++) {
        			Address selectedAddr = selected.get(i);
        			if (selectedAddr != null) {
	        			// Remove all unspents related to the address
	        			removeUnspentsForAddress(selectedAddr.getAddress());
        			}
        		}
        		addresses.removeAll(selected);
        		
        		// Temporary: Clear all unspents
        		displayFilteredUnspents();
    			btnRemoveAddress.setDisable(addresses.size() == 0);
        		btnRefreshUtxo.setDisable(addresses.size() == 0);
        		saveApplicationData();
        	}
        });
        
        btnRefreshUtxo = getVBoxGroupButton(utxoGroup, 1);
        btnRefreshUtxo.setDisable(true);
        btnRefreshUtxo.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				ObservableList<Address> selected = addressListView.getSelectionModel().getSelectedItems();
	    		refreshUtxo(selected);
			}
		});
        
        // Recipients group
        VBox recptGroup = buildTopVBoxGroup("Recipients", new String[] { "Add", "Remove" }, true);
        recptGroup.getChildren().add(recipientsTableView);
        recptGroup.setMaxHeight(Double.MAX_VALUE);
        Button btnAddRecipient = getVBoxGroupButton(recptGroup, 1);
        btnRemoveRecipient = getVBoxGroupButton(recptGroup, 2);
        btnRemoveRecipient.setDisable(true);
        
        btnAddRecipient.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				createAddRecipientDialog(primaryStage, recipients);
			}
		});
        
        btnRemoveRecipient.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				ObservableList<Recipient> selected = recipientsTableView.getSelectionModel().getSelectedItems();
				recipients.removeAll(selected);
				btnRemoveRecipient.setDisable(recipients.size() == 0);
				calculateTxOutput();
			}
		});
        
        addressListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Address>() {
			@Override
			public void changed(ObservableValue<? extends Address> arg0, Address arg1, Address arg2) {
				handleAddressSelectionChanged();
			}
		});
        
        utxoTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Unspent>() {
        	@Override
        	public void changed(ObservableValue<? extends Unspent> arg0, Unspent arg1, Unspent arg2) {
				handleUtxoSelectionChanged();
			}
		});
        
        recipientsTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Recipient>() {
			@Override
			public void changed(ObservableValue<? extends Recipient> arg0, Recipient arg1, Recipient arg2) {
				ObservableList<Recipient> selected = recipientsTableView.getSelectionModel().getSelectedItems();
				btnRemoveRecipient.setDisable(selected.size() == 0);
			}
		});
        
        btnGenerate.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				long balance = totalUnspentValue - totalOutputValue - FixedFeeValue;
				if (balance < 0) {
					// show error
					showAlert("Insufficient funds", "Oops! Something is wrong.",
							"You have not selected sufficient inputs to complete the transaction.", AlertType.ERROR);
					return;
				}
				
				String signedTx = generateSignedTransaction();
				if (signedTx != null && signedTx.trim().length() > 0) {
					createSignedTransactionDialog(primaryStage, signedTx);
				} else {
					showAlert("Transaction signing failed",
							"Oops! Something went wrong.",
							"The signed transaction could not be generated. This may be due to a decryption error. Please reimport your keys and try again.",
							AlertType.ERROR);
				}
			}
		});
        
        left.getChildren().add(addressGroup);
        left.getChildren().add(utxoGroup);
        right.getChildren().add(recptGroup);
        right.getChildren().add(bottom);
        right.getChildren().add(btnGenerate);
        
        root.getChildren().add(left);
        root.getChildren().add(right);
        
        primaryStage.setScene(new Scene(root, 800, 600));
        
        // Load data
        loadApplicationData();
        
        // Show the UI
        primaryStage.show();
    }
    
    private Button getVBoxGroupButton(VBox group, int btnIndex) {
    	HBox titleBox = (HBox) group.getChildren().get(0);
        Button btn = (Button) titleBox.getChildren().get(btnIndex);
        return btn;
    }
    
    private VBox buildTopVBoxGroup(String title, String[] buttonTexts, boolean hasAction) {
    	VBox groupBox = new VBox();
    	
        HBox titleBox = new HBox();
        titleBox.setPadding(new Insets(10, 10, 10, 10));
        titleBox.setAlignment(Pos.BASELINE_LEFT);
        titleBox.setSpacing(12);
        
        Label lblTitle = new Label();
        lblTitle.setText(title);
        titleBox.getChildren().add(lblTitle);
        
        if (hasAction) {
        	for (int i = 0; i < buttonTexts.length; i++) {
        		Button btn = new Button();
    	        btn.setText(buttonTexts[i]);
    	        titleBox.getChildren().add(btn);
	        }
        }
        
        groupBox.getChildren().add(titleBox);
        return groupBox;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static void initTableViewWithColumns(String[] columnNames, String[] propertyNames, int[] columnWidths,
    		TableView tableView) {
    	for (int i = 0; i < columnNames.length; i++) {
    		TableColumn col = new TableColumn(columnNames[i]);
    		col.setCellValueFactory(new PropertyValueFactory<>(propertyNames[i]));
    		col.setPrefWidth(columnWidths[i]);
    		if ("addressId".equals(propertyNames[i]) ||
    				"outputIndex".equals(propertyNames[i]) ||
    				"decimalValue".equals(propertyNames[i])) {
    			col.setStyle("-fx-alignment: BASELINE-RIGHT; -fx-padding: 0 12px 0 0");
    		}
    		tableView.getColumns().add(col);
    	}
    }
    
    private Stage createAddAddressDialog(Stage parent, ObservableList<Address> addressItems) {
    	Stage dialog = new Stage();
    	
    	VBox container = new VBox();
    	container.setPadding(new Insets(10));
    	container.setSpacing(10);
    	
    	Label lblWIF = new Label();
    	lblWIF.setText("Wallet Import Format private key");
    	TextField txtWIF = new TextField();
    	txtWIF.setPromptText("Enter WIF private key");
    	
    	HBox buttons = new HBox();
    	buttons.setAlignment(Pos.BASELINE_RIGHT);
    	buttons.setPrefWidth(200);
    	buttons.setSpacing(10);
    	
    	Button btnAdd = new Button();
    	btnAdd.setText("Import");
    	btnAdd.setPrefWidth(100);
    	
    	Button btnCancel = new Button();
    	btnCancel.setText("Cancel");
    	btnCancel.setPrefWidth(100);
    	buttons.getChildren().add(btnCancel);
    	buttons.getChildren().add(btnAdd);
    	
    	btnCancel.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
			public void handle(ActionEvent event) {
    			dialog.close();
    		}
    	});
    	
    	btnAdd.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String wif = txtWIF.getText().trim();
				try {
					Base58.decodeChecked(wif);
				} catch (AddressFormatException ex) {
					showAlert("Invalid WIF key.", "Oops! Something is wrong.", "The Wallet Import Format private key you entered is invalid.", AlertType.ERROR);
					return;
				}
				
				try {
					String pubkey = getPubKeyFromPrivateKey(wif);
					byte[] pubkeyBytes = Utils.hexToBytes(pubkey);
					String base58address = Utils.getAddressFromPublicKeyBytes(pubkeyBytes, 85);
					
					Address address = new Address();
					address.setId(getMaxAddressId(addressItems) + 1);
					address.setAddress(base58address);
					
					// encrypt the WIF
					address.setBase58WIF(Utils.encrypt(wif, getEncryptionKey()));
					addressItems.add(address);
					if (!addressItems.contains(address)) {
						// Prevent duplicate entries
						addressItems.add(address);
					}
					saveApplicationData();
				} catch (Exception ex) {
					showAlert("Invalid WIF key.", "The WIF could  not be imported.", "The Wallet Import Format private key you entered is invalid.", AlertType.ERROR);
					return;
				}
				
				dialog.close();
			}
		});
    	
    	container.getChildren().add(lblWIF);
    	container.getChildren().add(txtWIF);
    	container.getChildren().add(buttons);
    	
    	dialog.initStyle(StageStyle.UTILITY);
    	dialog.setScene(new Scene(container, 300, 120));
    	
    	dialog.initOwner(parent);
    	dialog.initModality(Modality.APPLICATION_MODAL); 
    	dialog.showAndWait();
    	
    	return dialog;
    }
    
    private Stage createAddRecipientDialog(Stage parent, ObservableList<Recipient> recipientItems) {
    	Stage dialog = new Stage();
    	dialog.initStyle(StageStyle.UTILITY);
    	
    	VBox container = new VBox();
    	container.setPadding(new Insets(10));
    	container.setSpacing(10);
    	
    	HBox buttons = new HBox();
    	buttons.setPadding(new Insets(20, 0, 0, 0));
    	buttons.setSpacing(10);
    	buttons.setPrefWidth(200);
    	buttons.setAlignment(Pos.BASELINE_RIGHT);
    	
    	Label lblAddress = new Label("LBRY address");
    	TextField txtAddress = new TextField();
    	txtAddress.setPromptText("Enter LBRY address");
    	Label lblAmount = new Label("Amount to send");
    	TextField txtAmount = new TextField();
    	txtAmount.setPromptText("Enter amount to send");
    	
    	Button btnAdd = new Button();
    	btnAdd.setText("Add");
    	btnAdd.setMaxWidth(Double.MAX_VALUE);
    	btnAdd.setPrefWidth(100);
    	
    	Button btnCancel = new Button();
    	btnCancel.setText("Cancel");
    	btnCancel.setPrefWidth(100);
    	buttons.getChildren().add(btnCancel);
    	buttons.getChildren().add(btnAdd);
    	
    	btnCancel.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
			public void handle(ActionEvent event) {
    			dialog.close();
    		}
    	});
    	
    	btnAdd.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String base58address = txtAddress.getText().trim();
				String amountString = txtAmount.getText().trim();
				
				if (base58address.length() != 34 || !base58address.startsWith("b")) {
					showAlert("Invalid Address.", "Oops! Something is wrong.", "The LBRY address you entered is invalid.", AlertType.ERROR);
					return;
				}
				
				try {
					Base58.decodeChecked(base58address);
				} catch (AddressFormatException ex) {
					showAlert("Invalid Address.", "Oops! Something is wrong.", "The LBRY address you entered is invalid.", AlertType.ERROR);
					return;
				}
				
				double value = 0;
				try {
					// Force 8 decimal places if higher
					value = Double.parseDouble(
								Utils.ZeroDecimalFormat.format(Double.parseDouble(amountString)).replaceAll(",", "")
							);
				} catch (NumberFormatException ex) {
					showAlert("Invalid Amount.", "Oops! Something is wrong.", "The amount you entered is invalid.", AlertType.ERROR);
					return;
				}
				
				Recipient recpt = new Recipient();
				recpt.setAddress(base58address);
				recpt.setAmount((long) (value * 100000000)); // unscaled value
				recpt.setDecimalValue(Utils.ZeroDecimalFormat.format(value));
				recipientItems.add(recpt);
				
				calculateTxOutput();
				
				dialog.close();
			}
		});
    	
    	container.getChildren().add(lblAddress);
    	container.getChildren().add(txtAddress);
    	container.getChildren().add(lblAmount);
    	container.getChildren().add(txtAmount);
    	container.getChildren().add(buttons);
    	
    	dialog.setScene(new Scene(container, 300, 200));
    	
    	dialog.initOwner(parent);
    	dialog.initModality(Modality.APPLICATION_MODAL); 
    	dialog.showAndWait();
    	
    	return dialog;
    }
    
    private Stage createSignedTransactionDialog(Stage parent, String signedTx) {
    	Stage dialog = new Stage(StageStyle.UTILITY);
    	dialog.setTitle("Signed transaction");
    	
    	GridPane container = new GridPane();
    	container.setPadding(new Insets(10));
    	
    	TextArea field = new TextArea();
    	field.setWrapText(true);
    	field.setMaxWidth(Double.MAX_VALUE);
    	field.setMaxHeight(Double.MAX_VALUE);
    	field.setEditable(false);
    	field.setText(signedTx);
    	
    	GridPane.setConstraints(field, 0, 0);
    	GridPane.setFillWidth(field, true);
    	GridPane.setFillHeight(field, true);
    	GridPane.setHgrow(field, Priority.ALWAYS);
    	GridPane.setVgrow(field, Priority.ALWAYS);
    	container.getChildren().add(field);
    	
    	Scene scene = new Scene(container, 600, 360);
    	dialog.setScene(scene);
    	dialog.initOwner(parent);
    	dialog.initModality(Modality.APPLICATION_MODAL); 
    	dialog.showAndWait();
    	
    	return dialog;
    }
    
    private int getMaxAddressId(ObservableList<Address> addressItems) {
    	int id = 0;
    	for (int i = 0; i < addressItems.size(); i++) {
    		id = Math.max(id, addressItems.get(i).getId());
    	}
    	return id;
    }
    
    private void showAlert(String title, String headerText, String message, Alert.AlertType alertType) {
    	Alert alert = createAlert(title, headerText, message, alertType);
    	alert.showAndWait();
    }
    
    public static Alert createAlert(String title, String headerText, String message, Alert.AlertType alertType) {
    	Alert alert = new Alert(alertType);
    	alert.setHeaderText(headerText);
        alert.setTitle(title);
        alert.setContentText(message);
        return alert;
    }
    
    private static List<Unspent> filteredUnspents(List<String> addresses) {
    	List<Unspent> filtered = new ArrayList<Unspent>();
    	for (HashMap.Entry<String, Unspent> entry : AllUnspents.entrySet()) {
    		Unspent unspent = entry.getValue();
    		// TODO: Show all unspents if no address selected? Or show none?
    		if (addresses == null || addresses.size() == 0 || addresses.indexOf(unspent.getAddress()) > -1) {
    			filtered.add(unspent);
    		}
    	}
    	
    	return filtered;
    }
    
    private void handleAddressSelectionChanged() {
    	ObservableList<Address> selected = addressListView.getSelectionModel().getSelectedItems();
    	btnRemoveAddress.setDisable(selected.size() == 0);
    	
    	if (loadUtxoInProgress) {
    		return;
    	}
    	btnRefreshUtxo.setDisable(selected.size() == 0);
    	
    	// automatically send the request if this is the first time
    	if (!IsFirstLoad && unspents.size() == 0) {
    		IsFirstLoad = true;
    		refreshUtxo(selected);
    	}
    }
    
    private void handleUtxoSelectionChanged() {
    	ObservableList<Unspent> selected = utxoTableView.getSelectionModel().getSelectedItems();
    	
    	totalUnspentValue = 0;
    	for (int i = 0; i < selected.size(); i++) {
    		Unspent unspent = selected.get(i);
    		if (unspent != null) {
    			totalUnspentValue += unspent.getValue();
    		}
    	}
    	
    	displayTotals();
    }
    
    private void displayTotals() {
    	lblUnspentValue.setText(Utils.ZeroDecimalFormat.format(Utils.getValueDecimal(totalUnspentValue)));
    	lblOutputValue.setText(Utils.ZeroDecimalFormat.format(Utils.getValueDecimal(totalOutputValue)));
    	
    	long balance = totalUnspentValue - totalOutputValue - FixedFeeValue;
    	lblBalanceValue.setTextFill(Color.web(balance < 0 ? "#ff0000" : "#000000"));
    	lblBalanceValue.setText(Utils.ZeroDecimalFormat.format(Utils.getValueDecimal(balance)));
    }
    
    private void calculateTxOutput() {
    	totalOutputValue = 0;
    	for (int i = 0; i < recipients.size(); i++) {
    		totalOutputValue += recipients.get(i).getAmount();
    	}
    	
    	displayTotals();
    }
    
    private void refreshUtxo(ObservableList<Address> selected) {
    	if (selected.size() == 0) {
    		return;
    	}
    	
    	// Bug fix: since this is a refresh, remove cached unspents for the selected addresses first
    	for (int i = 0; i < selected.size(); i++) {
    		Address address = selected.get(i);
    		if (address != null) {
    			removeUnspentsForAddress(address.getAddress());
    		}
    	}
    	
    	btnRefreshUtxo.setText("Loading...");
    	btnRefreshUtxo.setDisable(true);
    	loadUtxo(selected);
    }
    
    private void finishLoadUtxo() {
    	btnRefreshUtxo.setText("Refresh");
    	btnRefreshUtxo.setDisable(false);
    	
    	// parse the current result
    	loadUtxoInProgress = false;
    	if (currentUtxoResult == null || currentUtxoResult.isNull() ||
    			!Utils.getJsonBoolean("success", currentUtxoResult) ||
    			!currentUtxoResult.has("utxo")) {
    		showAlert("Could not load unspent outputs", "Oops! Something went wrong",
    				"The unspent outputs could not be loaded for the selected address(es)", AlertType.ERROR);
    		return;
    	}
    	
    	 JsonNode utxoNode = currentUtxoResult.get("utxo");
    	 if (!utxoNode.isArray()) {
    		 // show an error
    		 showAlert("Invalid result returned", "Oops! Something went wrong",
     				"The data returned for the request was invalid", AlertType.ERROR);
    		 return;
    	 }
    	 
    	 for (int i = 0; i < utxoNode.size(); i++) {
    		 Unspent unspent = Unspent.fromJsonNode(utxoNode.get(i));
    		 int addressId = getIdForAddress(unspent.getAddress(), addresses);
    		 if (addressId < 0) {
    			 continue;
    		 }
    		 unspent.setAddressId(addressId);
    		 AllUnspents.put(unspent.toString(), unspent);
    	 }
    	 saveApplicationData();
    	 displayFilteredUnspents();
    }
    
    public void displayFilteredUnspents() {
    	List<Address> selectedAddresses = new ArrayList<Address>(addressListView.getSelectionModel().getSelectedItems());
    	List<String> base58Addresses = new ArrayList<String>();
    	for (Address address : selectedAddresses) {
    		base58Addresses.add(address.getAddress());
    	}
    	
    	List<Unspent> filtered = filteredUnspents(base58Addresses);
    	unspents.clear();
    	unspents.addAll(filtered);
    }
    
    public void removeUnspentsForAddress(String address) {
    	List<String> keysToRemove = new ArrayList<String>();
    	for (HashMap.Entry<String, Unspent> entry : AllUnspents.entrySet()) {
    		if (address.equals(entry.getValue().getAddress())) {
    			keysToRemove.add(entry.getKey());
    		}
    	}
    	for (String key : keysToRemove) {
    		AllUnspents.remove(key);
    	}
    	
    	saveApplicationData();
    }
    
    public static int getIdForAddress(String address, ObservableList<Address> addressItems) {
    	for (int i = 0; i < addressItems.size(); i++) {
    		Address item = addressItems.get(i);
    		if (address.equals(item.getAddress())) {
    			return item.getId();
    		}
    	}
    	
    	return -1;
    }
    
    public void loadUtxo(ObservableList<Address> addresses) {
    	if (loadUtxoInProgress) {
    		return;
    	}
    	
    	// get the selected addresses
    	List<String> addressList = new ArrayList<String>();
    	for (Address address : addresses) {
    		if (address != null) {
    			addressList.add(address.getAddress());
    		}
    	}
    	String param = String.join(",", addressList);
    	String url = UtxoUrl.replace("{param}", param); 
    	
    	loadUtxoInProgress = true;
    	Runnable runnable = new Runnable() {
			@Override
			public void run() {
				try {
					currentUtxoResult = httpGet(url);
				} catch (Exception ex) {
					currentUtxoResult = null;
				}
				
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						finishLoadUtxo();
					}
				});
			}
		};
		new Thread(runnable).start();
    }
    
    private String generateSignedTransaction() {
    	HashMap<String, Address> addressMap = new HashMap<String, Address>();
    	
    	// Populate address map
    	for (int i = 0; i < addresses.size(); i++) {
    		Address address = addresses.get(i);
    		addressMap.put(address.getAddress(), address);
    	}

        BigInteger amountToSendBI = BigInteger.valueOf(totalOutputValue);
        BigInteger totalUsableUnspent = BigInteger.valueOf(0);
    	
    	// Build usable unspents from the list of selected unspents
    	ObservableList<Unspent> usableUnspents = utxoTableView.getSelectionModel().getSelectedItems();
    	if (usableUnspents.size() == 0) {
    		// Display error that no utxo is selected
    		showAlert("No inputs selected", "Oops! Something is wrong",
    				"You did not select any inputs to complete the transaction.", AlertType.ERROR);
    		return null;
    	}
    	
    	for (Unspent unspent : usableUnspents) {
    		totalUsableUnspent = totalUsableUnspent.add(BigInteger.valueOf(unspent.getValue()));
    	}
    	
    	if (recipients.size() == 0) {
    		showAlert("No recipients", "Oops! Something is wrong",
    				"You did not specify any recipients to receive the LBRY credits.", AlertType.ERROR);
    		return null;
    	}
    	
    	List<RawInput> rawInputs = buildRawInputsFromUnspents(usableUnspents, addressMap);
    	List<RawOutput> rawOutputs = buildRawOutputs(
            recipients, amountToSendBI, totalUsableUnspent, usableUnspents, FixedFeeValue,
            txtReturnAddress.getText().trim());
    	
    	RawTransaction tx = RawTransaction.createRawTransaction(rawInputs, rawOutputs, "85");
        String signedTransaction = RawTransaction.signRawTransaction(tx, CurrentWifKeys, CurrentPubKeys);
        return signedTransaction != null ? signedTransaction.toLowerCase() : null;
    }
    
    private List<RawInput> buildRawInputsFromUnspents(List<Unspent> usableUnspents, HashMap<String, Address> addressMap) {
        CurrentWifKeys = new String[usableUnspents.size()];
        CurrentPubKeys = new String[usableUnspents.size()];
        List<RawInput> rawInputs = new ArrayList<RawInput>();
        for (int i = 0; i < usableUnspents.size(); i++) {
            Unspent unspent = usableUnspents.get(i);

            RawInput rawInput = RawInput.rawInputFromUnspent(unspent);
            rawInput.setSequence(i + 1);
            rawInputs.add(rawInput);

            String base58address = unspent.getAddresses().get(0);
            Address walletAddress = addressMap.get(base58address);
            
            CurrentWifKeys[i] = Utils.decrypt(walletAddress.getBase58WIF(), getEncryptionKey());
            CurrentPubKeys[i] = getPubKeyFromPrivateKey(CurrentWifKeys[i]);
        }

        return rawInputs;
    }
    
    public static String getPubKeyFromPrivateKey(String base58wif) {
    	byte[] privKeyBytes = new byte[32];
    	byte[] checkedPrivKeyBytes = Base58.decodeChecked(base58wif);

        // support for compressed keys
        boolean isCompressed = (checkedPrivKeyBytes.length == 34 && checkedPrivKeyBytes[checkedPrivKeyBytes.length - 1] == 0x01);
        System.arraycopy(checkedPrivKeyBytes, 1, privKeyBytes, 0, privKeyBytes.length);
        
    	BigInteger privateKeyBN = new BigInteger(1, privKeyBytes);
        ECPoint curvePt = Utils.CURVE.getG().multiply(privateKeyBN);
        byte[] publicKeyBytes = curvePt.getEncoded(isCompressed);
        String publicKeyHex = Utils.bytesToHex(publicKeyBytes);
        return publicKeyHex;
    }

    private List<RawOutput> buildRawOutputs(ObservableList<Recipient> recipients,
        BigInteger amountToSendBI,
        BigInteger totalUsableUnspentValue,
        ObservableList<Unspent> usableUnspents,
        long transactionFee,
        String returnAddress) {
        
    	
        // Minimum output to check for fees
        long minOutput = 0;
        List<RawOutput> rawOutputs = new ArrayList<RawOutput>();
        for (int i = 0; i < recipients.size(); i++) {
        	Recipient recipient = recipients.get(i);
            RawOutput recptOutput = new RawOutput();
            recptOutput.setAddress(recipient.getAddress());
            recptOutput.setValue(recipient.getAmount());
            rawOutputs.add(recptOutput);

            if (minOutput == 0) {
                minOutput = recptOutput.getValue();
            } else {
                minOutput = Math.min(minOutput, recptOutput.getValue());
            }
        }

        if (totalUsableUnspentValue.longValue() - amountToSendBI.longValue() - FixedFeeValue > 0) {
            String changeOutputAddress = (returnAddress != null && returnAddress.trim().length() > 0) ?
            		returnAddress :
            		usableUnspents.get(0).getAddresses().get(0); // return change to the first unspent address
            											         // if no return address specified
            // Create change output
            RawOutput changeOutput = new RawOutput();
            // Set the change output address to the first usable unspent address
            changeOutput.setAddress(changeOutputAddress);
            changeOutput.setValue(totalUsableUnspentValue.longValue() - amountToSendBI.longValue() - transactionFee);
            minOutput = Math.min(minOutput, changeOutput.getValue());
            rawOutputs.add(changeOutput);
        }

        return rawOutputs;
    }
}
